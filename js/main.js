var Validation;
(function (Validation) {
    (function (Gender) {
        Gender[Gender["Man"] = 0] = "Man";
        Gender[Gender["Woman"] = 1] = "Woman";
        Gender[Gender["Unspecified"] = 2] = "Unspecified";
    })(Validation.Gender || (Validation.Gender = {}));
    var Gender = Validation.Gender;
})(Validation || (Validation = {}));
///<reference path="../gender.ts"/>
var FormDecorator;
(function (FormDecorator) {
    var Decorator = (function () {
        function Decorator() {
        }
        Decorator.withError = function (field, msg) {
            Decorator.removeMessages(field);
            var n = document.createElement("label");
            field.classList.remove('with-success');
            field.classList.add('with-error');
            field.classList.add('animated rubberBand');
            window.setTimeout(function () {
                field.classList.remove('animated rubberBand');
            }, 200);
            n.classList.add('error');
            n.setAttribute('for', '#' + field.id);
            n.innerHTML = msg;
            n.style.color = 'red';
            field.parentElement.appendChild(n);
        };
        Decorator.withSuccess = function (field, msg) {
            Decorator.removeMessages(field);
            var n = document.createElement("label");
            field.classList.remove('with-error');
            field.classList.add('with-success');
            n.classList.add('success');
            n.setAttribute('for', '#' + field.id);
            n.innerHTML = msg;
            n.style.color = 'green';
            // field.parentElement.appendChild(n);
        };
        Decorator.removeMessages = function (elem) {
            var label = elem.parentElement.querySelector('[for="#' + elem.id + '"]');
            if (label !== null)
                label.remove();
        };
        return Decorator;
    }());
    FormDecorator.Decorator = Decorator;
})(FormDecorator || (FormDecorator = {}));
///<reference path="../decorator/decorator.ts"/>
var Validation;
(function (Validation) {
    var Decorator = FormDecorator.Decorator;
    var PatternField = (function () {
        function PatternField(element) {
            this.element = element;
            if (this.element.hasAttribute('data-pattern')) {
                this.setPattern(this.element.getAttribute('data-pattern'));
            }
            if (this.element.hasAttribute('data-message')) {
                this.message = this.element.getAttribute('data-message');
            }
            else {
                this.message = "Pole ma nieprawidłową wartość";
            }
        }
        PatternField.prototype.getElement = function () {
            return this.element;
        };
        PatternField.prototype.getName = function () {
            return this.getElement().name;
        };
        PatternField.prototype.getType = function () {
            return 'pattern';
        };
        PatternField.prototype.setPattern = function (pattern) {
            this.pattern = new RegExp(pattern);
        };
        PatternField.prototype.getPattern = function () {
            return this.pattern;
        };
        PatternField.prototype.isValid = function () {
            return this.pattern.test(this.getValue());
        };
        PatternField.prototype.getValue = function () {
            return this.element.value;
        };
        PatternField.prototype.getMessage = function () {
            return this.message;
        };
        PatternField.prototype.validate = function () {
            if (this.isValid()) {
                return Decorator.withSuccess(this.element, "Poprawna wartość pola");
            }
            else {
                return Decorator.withError(this.element, this.getMessage());
            }
        };
        return PatternField;
    }());
    Validation.PatternField = PatternField;
})(Validation || (Validation = {}));
///<reference path="../gender.ts"/>
///<reference path="../decorator/decorator.ts"/>
var Validation;
(function (Validation) {
    var Decorator = FormDecorator.Decorator;
    var PeselField = (function () {
        function PeselField(element) {
            this.element = element;
        }
        PeselField.prototype.getValue = function () {
            return this.element.value;
        };
        PeselField.prototype.getElement = function () {
            return this.element;
        };
        PeselField.prototype.getType = function () {
            return 'pesel';
        };
        PeselField.prototype.getName = function () {
            return this.getElement().name;
        };
        //region Gender functions
        PeselField.prototype.getGender = function () {
            if (this.getValue().length < 9) {
                return Validation.Gender.Unspecified;
            }
            if (parseInt(this.getValue().charAt(9)) % 2) {
                return Validation.Gender.Man;
            }
            else {
                return Validation.Gender.Woman;
            }
        };
        PeselField.prototype.isMan = function () {
            return this.getGender() === Validation.Gender.Man;
        };
        PeselField.prototype.isWoman = function () {
            return this.getGender() === Validation.Gender.Woman;
        };
        //endregion
        //region Date functions
        PeselField.prototype.hasDate = function () {
            return (this.getValue().length >= 6);
        };
        PeselField.prototype.getYear = function () {
            var year = parseInt(this.getValue().substr(0, 2));
            var month = parseInt(this.getValue().substr(2, 2));
            if (month < 20) {
                return 1900 + year;
            }
            else if (month < 40) {
                return 2000 + year;
            }
            else if (month < 60) {
                return 2100 + year;
            }
            else if (month < 80) {
                return 2200 + year;
            }
            else {
                return 1800 + year;
            }
        };
        PeselField.prototype.getMonth = function () {
            var month = parseInt(this.getValue().substr(2, 2));
            while (month > 20) {
                month -= 20;
            }
            if (month > 12) {
                throw new Error("Miesiąc nie może być większy niż 12");
            }
            return month;
        };
        PeselField.prototype.getDay = function () {
            var day = parseInt(this.getValue().substr(4, 2));
            if (day > 31)
                throw new Error("Dzień nie może być większy niż 31");
            return day;
        };
        PeselField.prototype.getDate = function () {
            if (!this.hasDate()) {
                throw new Error("Pesel ma nieprawidłową długość");
            }
            var date = new Date();
            date.setFullYear(this.getYear(), this.getMonth() - 1, this.getDay());
            return date;
        };
        //endregion
        PeselField.prototype.isValid = function () {
            try {
                this.getDate();
            }
            catch (e) {
                return false;
            }
            return /^\d{11}$/.test(this.getValue());
        };
        PeselField.prototype.validate = function () {
            try {
                this.getDate();
            }
            catch (e) {
                return Decorator.withError(this.element, e.message);
            }
            if (!/^\d{11}$/.test(this.getValue())) {
                return Decorator.withError(this.element, "Pesel ma nieprawidłową długość");
            }
            return Decorator.withSuccess(this.element, "PESEL poprawny");
        };
        return PeselField;
    }());
    Validation.PeselField = PeselField;
})(Validation || (Validation = {}));
///<reference path="../decorator/decorator.ts"/>
var Validation;
(function (Validation) {
    var Decorator = FormDecorator.Decorator;
    var PasswordField = (function () {
        function PasswordField(element) {
            this.element = element;
            if (this.element.hasAttribute('data-strength')) {
                this.level = parseInt(this.element.getAttribute('data-strength'));
                if (this.level > PasswordField.levels.length)
                    this.level = PasswordField.levels.length - 1;
            }
            else {
                this.level = 0;
            }
        }
        PasswordField.prototype.getName = function () {
            return this.getElement().name;
        };
        PasswordField.prototype.getType = function () {
            return 'password';
        };
        PasswordField.prototype.getElement = function () {
            return this.element;
        };
        PasswordField.prototype.isValid = function () {
            // iterate over all levels
            for (var i = 0; i <= this.level; i++) {
                // iterate over all regex values in that level
                if (PasswordField.levels[i].length > 0)
                    for (var _i = 0, _a = PasswordField.levels[i]; _i < _a.length; _i++) {
                        var regex = _a[_i];
                        if (!regex.test(this.getValue()))
                            return false;
                    }
            }
            // all of the required regex tests passed
            return true;
        };
        PasswordField.prototype.getValue = function () {
            return this.element.value;
        };
        PasswordField.prototype.validate = function () {
            if (this.isValid()) {
                Decorator.withSuccess(this.element, "Hasło odpowiednio skomplikowane");
            }
            else {
                Decorator.withError(this.element, "Hasło nie jest wystarczająco skomplikowane");
            }
        };
        PasswordField.levels = [
            [/^.{3,30}$/],
            [/[!@#$%\^&*()]/, /[a-z]/, /[A-Z]/, /[0-9]/]
        ];
        return PasswordField;
    }());
    Validation.PasswordField = PasswordField;
})(Validation || (Validation = {}));
///<reference path="../decorator/decorator.ts"/>
var Validation;
(function (Validation) {
    var Decorator = FormDecorator.Decorator;
    var RepeatField = (function () {
        function RepeatField(element) {
            this.element = element;
            if (this.element.hasAttribute('data-repeat')) {
                var parent_1 = document.querySelector(this.element.getAttribute('data-repeat'));
                this.parent = parent_1;
            }
        }
        RepeatField.prototype.getElement = function () {
            return this.element;
        };
        RepeatField.prototype.isValid = function () {
            if (!this.hasParent())
                return false;
            return (this.getValue() === this.parent.value);
        };
        RepeatField.prototype.getType = function () {
            return 'repeat';
        };
        RepeatField.prototype.getName = function () {
            return this.getElement().name;
        };
        RepeatField.prototype.getValue = function () {
            return this.element.value;
        };
        RepeatField.prototype.validate = function () {
            if (this.isValid()) {
                return Decorator.withSuccess(this.element, "Hasła są takie same");
            }
            else {
                return Decorator.withError(this.element, "Hasła są różne");
            }
        };
        RepeatField.prototype.hasParent = function () {
            return typeof this.parent !== "undefined";
        };
        return RepeatField;
    }());
    Validation.RepeatField = RepeatField;
})(Validation || (Validation = {}));
///<reference path="../defs/es6-promise.d.ts"/>
///<reference path="../defs/jquery.d.ts"/>
///<reference path="../decorator/decorator.ts"/>
var Validation;
(function (Validation) {
    var Decorator = FormDecorator.Decorator;
    var ApiField = (function () {
        function ApiField(element) {
            this.valid = undefined;
            this.timer = undefined;
            this.element = element;
            if (this.element.hasAttribute('data-source')) {
                this.source = this.element.getAttribute('data-source');
            }
        }
        ApiField.prototype.getName = function () {
            return this.getElement().name;
        };
        ApiField.prototype.getType = function () {
            return 'api';
        };
        ApiField.prototype.getElement = function () {
            return this.element;
        };
        ApiField.prototype.isValid = function () {
            return this.valid !== undefined && this.valid;
        };
        ApiField.prototype.getValue = function () {
            return this.element.value;
        };
        ApiField.prototype.validate = function () {
            var _this = this;
            if (this.timer === undefined)
                this.timer = window.setTimeout(function () {
                    _this.testApi().then(function (response) {
                        _this.timer = undefined;
                        if (response) {
                            Decorator.withSuccess(_this.element, 'Nazwa poprawna i wolna');
                        }
                        else {
                            Decorator.withError(_this.element, 'Nazwa jest zajęta');
                        }
                    }, function (error) {
                        _this.timer = undefined;
                        console.log(error);
                        Decorator.withError(_this.element, 'Błąd podczas komunikacji z api');
                    });
                }, 300);
        };
        ApiField.prototype.getUrl = function () {
            return this.source + this.getValue();
        };
        ApiField.prototype.testApi = function () {
            var _this = this;
            this.valid = undefined;
            return new Promise(function (resolve, reject) {
                $.get(_this.getUrl(), {}, function (data) {
                    for (var i in data) {
                        resolve(data[i]);
                    }
                });
            }).then(function () { return _this.valid = true; }, function () { return _this.valid = false; });
        };
        return ApiField;
    }());
    Validation.ApiField = ApiField;
})(Validation || (Validation = {}));
var Validation;
(function (Validation) {
    var GenderField = (function () {
        function GenderField(element) {
            this.elements = [];
            for (var _i = 0, _a = element; _i < _a.length; _i++) {
                var elem = _a[_i];
                this.elements.push(elem);
            }
        }
        GenderField.prototype.getName = function () {
            return 'sex';
        };
        GenderField.prototype.getType = function () {
            return 'gender';
        };
        GenderField.prototype.getElement = function () {
            return null;
        };
        GenderField.prototype.isValid = function () {
            for (var _i = 0, _a = this.elements; _i < _a.length; _i++) {
                var elem = _a[_i];
                if (elem.checked)
                    return true;
            }
            return false;
        };
        GenderField.prototype.getValue = function () {
            return this.elements.filter(function (radio) { return radio.checked; })[0].value;
        };
        GenderField.prototype.getGender = function () {
            var map = { 'man': Validation.Gender.Man, 'woman': Validation.Gender.Woman };
            for (var _i = 0, _a = this.elements; _i < _a.length; _i++) {
                var elem = _a[_i];
                if (elem.checked) {
                    return map[elem.value];
                }
            }
            return Validation.Gender.Unspecified;
        };
        GenderField.prototype.setGender = function (gender) {
            var map = { 'man': Validation.Gender.Man, 'woman': Validation.Gender.Woman };
            for (var key in map) {
                if (map[key] === gender) {
                    return this.markChecked(key);
                }
            }
        };
        GenderField.prototype.setMale = function () {
            this.setGender(Validation.Gender.Man);
        };
        GenderField.prototype.markChecked = function (gender) {
            var toCheck = null;
            for (var _i = 0, _a = this.elements; _i < _a.length; _i++) {
                var elem = _a[_i];
                if (elem.value === gender)
                    toCheck = elem;
                elem.checked = false;
            }
            if (toCheck !== null)
                toCheck.checked = true;
        };
        GenderField.prototype.getMessage = function () {
            return "Płeć niepoprawna";
        };
        GenderField.prototype.validate = function () {
            if (this.isValid()) {
            }
            else {
            }
        };
        return GenderField;
    }());
    Validation.GenderField = GenderField;
})(Validation || (Validation = {}));
///<reference path="validatableField.ts"/>
///<reference path="patternField.ts"/>
///<reference path="peselField.ts"/>
///<reference path="passwordField.ts"/>
///<reference path="repeatField.ts"/>
///<reference path="apiField.ts"/>
///<reference path="genderField.ts"/>
///<reference path="../decorator/decorator.ts"/>
var Validation;
(function (Validation) {
    var Validator = (function () {
        function Validator(selector) {
            this.fields = [];
            for (var _i = 0, _a = document.querySelectorAll(selector); _i < _a.length; _i++) {
                var field = _a[_i];
                var element = void 0;
                switch (field.attributes.getNamedItem('data-type').value) {
                    case 'pattern':
                        this.addField(new Validation.PatternField(field));
                        break;
                    case 'pesel':
                        this.addField(new Validation.PeselField(field));
                        break;
                    case 'password':
                        this.addField(new Validation.PasswordField(field));
                        break;
                    case 'repeat':
                        this.addField(new Validation.RepeatField(field));
                        break;
                    case 'api':
                        this.addField(new Validation.ApiField(field));
                        break;
                    case 'gender':
                        this.addField(new Validation.GenderField(field.querySelectorAll('.form-control')), false);
                        break;
                }
            }
        }
        Validator.prototype.addField = function (field, listen) {
            if (listen === void 0) { listen = true; }
            if (typeof listen !== "undefined" && field.getElement() !== null)
                field.getElement().addEventListener('keyup', function () { return field.validate(); });
            this.fields.push(field);
        };
        Validator.prototype.isValid = function () {
            for (var _i = 0, _a = this.fields; _i < _a.length; _i++) {
                var field = _a[_i];
                if (!field.isValid())
                    return false;
            }
            return true;
        };
        Validator.prototype.validate = function () {
            for (var _i = 0, _a = this.fields; _i < _a.length; _i++) {
                var field = _a[_i];
                field.validate();
            }
        };
        Validator.prototype.getElements = function (type) {
            var elements = [];
            for (var _i = 0, _a = this.fields; _i < _a.length; _i++) {
                var field = _a[_i];
                if (field.getType() === type) {
                    elements.push(field);
                }
            }
            return elements;
        };
        Validator.prototype.getData = function () {
            var data = {};
            for (var _i = 0, _a = this.fields; _i < _a.length; _i++) {
                var field = _a[_i];
                data[field.getName()] = field.getValue();
            }
            return data;
        };
        return Validator;
    }());
    Validation.Validator = Validator;
})(Validation || (Validation = {}));
///<reference path="validation/validator.ts"/>
var Validator = Validation.Validator;
var GenderField = Validation.GenderField;
var PeselField = Validation.PeselField;
var ApiField = Validation.ApiField;
var App = (function () {
    function App() {
        var _this = this;
        this.validator = new Validator('.validate');
        var peselField = this.validator.getElements('pesel')[0];
        var genderField = this.validator.getElements('gender')[0];
        peselField.getElement().addEventListener('keyup', function () { return genderField.setGender(peselField.getGender()); });
        var form = document.querySelector('form');
        form.addEventListener('submit', function (e) {
            if (!_this.validator.isValid()) {
                e.preventDefault();
                return false;
            }
        });
    }
    App.prototype.getData = function () {
        return $('form');
    };
    return App;
}());
var app = new App();
//# sourceMappingURL=main.js.map