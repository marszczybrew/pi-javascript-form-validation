///<reference path="validation/validator.ts"/>
import Validator = Validation.Validator;
import GenderField = Validation.GenderField;
import PeselField = Validation.PeselField;
import ApiField = Validation.ApiField;

class App {
    private validator: Validator;

    constructor() {
        this.validator = new Validator('.validate');

        let peselField = <PeselField>this.validator.getElements('pesel')[0];
        let genderField = <GenderField>this.validator.getElements('gender')[0];

        peselField.getElement().addEventListener('keyup', () => genderField.setGender(peselField.getGender()));

        let form = document.querySelector('form');
        form.addEventListener('submit', (e) => {
            if (!this.validator.isValid()) {
                e.preventDefault();
                return false;
            }
        });
    }

    private getData() {
        return $('form');
    }
}

let app = new App();
