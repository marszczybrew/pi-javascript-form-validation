namespace Validation {
    import Decorator = FormDecorator.Decorator;
    export class GenderField implements ValidatableField {
        private elements: HTMLInputElement[] = [];

        constructor(element: NodeList) {
            for (let elem of <HTMLInputElement[]><any>element) {
                this.elements.push(elem);
            }
        }

        public getName():string {
            return 'sex';
        }

        public getType():string {
            return 'gender';
        }

        public getElement(): HTMLInputElement {
            return null;
        }

        public isValid(): boolean {
            for (let elem of this.elements) {
                if (elem.checked) return true;
            }
            return false;
        }

        public getValue(): string {
            return this.elements.filter((radio: HTMLInputElement) => radio.checked)[0].value;
        }

        public getGender(): Gender {
            let map = {'man': Gender.Man, 'woman': Gender.Woman};
            for (let elem of this.elements) {
                if (elem.checked) {
                    return map[elem.value];
                }
            }
            return Gender.Unspecified;
        }

        public setGender(gender: Gender): void {
            let map = {'man': Gender.Man, 'woman': Gender.Woman};
            for (let key in map) {
                if (map[key] === gender) {
                    return this.markChecked(key);
                }
            }
        }

        public setMale() {
            this.setGender(Gender.Man);
        }

        private markChecked(gender: string) {
            let toCheck = null;
            for (let elem of this.elements) {
                if (elem.value === gender) toCheck = elem;
                elem.checked = false;
            }
            if (toCheck !== null)
                toCheck.checked = true;
        }

        public getMessage(): string {
            return "Płeć niepoprawna";
        }

        public validate(): void {
            if (this.isValid()) {
            } else {
            }
        }
    }
}