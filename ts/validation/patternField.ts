///<reference path="../decorator/decorator.ts"/>

namespace Validation {
    import Decorator = FormDecorator.Decorator;
    export class PatternField implements ValidatableField {
        private element: HTMLInputElement;
        private pattern: RegExp;
        private message: string;

        constructor(element: Node) {
            this.element = <HTMLInputElement> element;

            if (this.element.hasAttribute('data-pattern')) {
                this.setPattern(this.element.getAttribute('data-pattern'));
            }

            if (this.element.hasAttribute('data-message')) {
                this.message = this.element.getAttribute('data-message');
            } else {
                this.message = "Pole ma nieprawidłową wartość";
            }
        }

        public getElement(): HTMLInputElement {
            return this.element;
        }

        public getName(): string {
            return this.getElement().name;
        }

        public getType():string {
            return 'pattern';
        }

        public setPattern(pattern: string) {
            this.pattern = new RegExp(pattern);
        }

        public getPattern(): RegExp {
            return this.pattern;
        }

        public isValid(): boolean {
            return this.pattern.test(this.getValue());
        }

        public getValue(): string {
            return this.element.value;
        }

        public getMessage(): string {
            return this.message;
        }

        public validate(): void {
            if (this.isValid()) {
                return Decorator.withSuccess(this.element, "Poprawna wartość pola");
            } else {
                return Decorator.withError(this.element, this.getMessage());
            }
        }
    }
}