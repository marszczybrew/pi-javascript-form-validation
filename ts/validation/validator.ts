///<reference path="validatableField.ts"/>
///<reference path="patternField.ts"/>
///<reference path="peselField.ts"/>
///<reference path="passwordField.ts"/>
///<reference path="repeatField.ts"/>
///<reference path="apiField.ts"/>
///<reference path="genderField.ts"/>
///<reference path="../decorator/decorator.ts"/>

namespace Validation {
    export class Validator {
        private fields: ValidatableField[] = [];

        constructor(selector: string) {
            for (let field of <Node[]><any>document.querySelectorAll(selector)) {
                let element: ValidatableField;
                switch (field.attributes.getNamedItem('data-type').value) {
                    case 'pattern':
                        this.addField(new PatternField(field));
                        break;
                    case 'pesel':
                        this.addField(new PeselField(field));
                        break;
                    case 'password':
                        this.addField(new PasswordField(field));
                        break;
                    case 'repeat':
                        this.addField(new RepeatField(field));
                        break;
                    case 'api':
                        this.addField(new ApiField(field));
                        break;
                    case 'gender':
                        this.addField(new GenderField((<HTMLElement>field).querySelectorAll('.form-control')), false);
                        break;
                }
            }
        }

        private addField(field: ValidatableField, listen: boolean = true): void {
            if (typeof listen !== "undefined" && field.getElement() !== null)
                field.getElement().addEventListener('keyup', () => field.validate());
            this.fields.push(field);
        }

        public isValid(): boolean {
            for (let field of this.fields) {
                if (!field.isValid())
                    return false;
            }
            return true;
        }

        public validate(): void {
            for (let field of this.fields) {
                field.validate();
            }
        }

        public getElements(type: string) {
            let elements = [];
            for (let field of this.fields) {
                if (field.getType() === type) {
                    elements.push(field)
                }
            }
            return elements;
        }

        public getData() {
            let data = {};
            for(let field of this.fields) {
                data[field.getName()] = field.getValue();
            }
            return data;
        }
    }
}