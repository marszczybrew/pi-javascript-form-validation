///<reference path="../gender.ts"/>
namespace Validation {
    export interface ValidatableField {
        isValid(): boolean;
        getValue(): string;
        validate(): void;
        getElement(): HTMLInputElement;
        getType(): string;
        getName(): string;
    }
}