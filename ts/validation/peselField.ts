///<reference path="../gender.ts"/>
///<reference path="../decorator/decorator.ts"/>

namespace Validation {
    import Decorator = FormDecorator.Decorator;
    export class PeselField implements ValidatableField {
        private element;

        constructor(element: Node) {
            this.element = element;
        }

        public getValue(): string {
            return this.element.value;
        }

        public getElement(): HTMLInputElement {
            return this.element;
        }

        public getType(): string {
            return 'pesel';
        }

        public getName(): string {
            return this.getElement().name;
        }

        //region Gender functions
        public getGender(): Gender {
            if (this.getValue().length < 9) {
                return Gender.Unspecified;
            }

            if (parseInt(this.getValue().charAt(9)) % 2) {
                return Gender.Man;
            } else {
                return Gender.Woman;
            }
        }

        public isMan(): boolean {
            return this.getGender() === Gender.Man;
        }

        public isWoman(): boolean {
            return this.getGender() === Gender.Woman;
        }

        //endregion

        //region Date functions
        private hasDate(): boolean {
            return (this.getValue().length >= 6);
        }

        private getYear(): number {
            let year = parseInt(this.getValue().substr(0, 2));
            let month = parseInt(this.getValue().substr(2, 2));
            if (month < 20) {
                return 1900 + year;
            } else if (month < 40) {
                return 2000 + year;
            } else if (month < 60) {
                return 2100 + year;
            } else if (month < 80) {
                return 2200 + year;
            } else {
                return 1800 + year;
            }
        }

        private getMonth(): number {
            let month = parseInt(this.getValue().substr(2, 2));
            while (month > 20) {
                month -= 20;
            }
            if (month > 12) {
                throw new Error("Miesiąc nie może być większy niż 12");
            }
            return month;
        }

        private getDay(): number {
            let day = parseInt(this.getValue().substr(4, 2));
            if (day > 31)
                throw new Error("Dzień nie może być większy niż 31");
            return day;
        }

        public getDate(): Date {
            if (!this.hasDate()) {
                throw new Error("Pesel ma nieprawidłową długość");
            }
            let date = new Date();
            date.setFullYear(this.getYear(), this.getMonth() - 1, this.getDay());
            return date;
        }

        //endregion

        public isValid(): boolean {
            try {
                this.getDate()
            } catch (e) {
                return false;
            }

            return /^\d{11}$/.test(this.getValue());
        }

        public validate(): void {
            try {
                this.getDate()
            } catch (e) {
                return Decorator.withError(this.element, e.message);
            }

            if (!/^\d{11}$/.test(this.getValue())) {
                return Decorator.withError(this.element, "Pesel ma nieprawidłową długość");
            }

            return Decorator.withSuccess(this.element, "PESEL poprawny");
        }
    }
}