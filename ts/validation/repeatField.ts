///<reference path="../decorator/decorator.ts"/>

namespace Validation {
    import Decorator = FormDecorator.Decorator;
    export class RepeatField implements ValidatableField {
        private element: HTMLInputElement;
        private parent: HTMLInputElement;

        public getElement(): HTMLInputElement {
            return this.element;
        }

        public isValid(): boolean {
            if (!this.hasParent())
                return false;
            return (this.getValue() === this.parent.value);
        }

        public getType():string{
            return 'repeat';
        }

        public getName(): string {
            return this.getElement().name;
        }

        public getValue(): string {
            return this.element.value;
        }

        public validate(): void {
            if (this.isValid()) {
                return Decorator.withSuccess(this.element, "Hasła są takie same");
            } else {
                return Decorator.withError(this.element, "Hasła są różne");
            }
        }

        private hasParent() {
            return typeof this.parent !== "undefined";
        }

        public constructor(element: Node) {
            this.element = <HTMLInputElement> element;
            if (this.element.hasAttribute('data-repeat')) {
                let parent = document.querySelector(this.element.getAttribute('data-repeat'));
                this.parent = <HTMLInputElement>parent;
            }
        }
    }
}