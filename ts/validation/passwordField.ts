///<reference path="../decorator/decorator.ts"/>

namespace Validation {
    import Decorator = FormDecorator.Decorator;
    export class PasswordField implements ValidatableField {
        private element: HTMLInputElement;
        private level: number;

        public constructor(element: Node) {
            this.element = <HTMLInputElement>element;
            if (this.element.hasAttribute('data-strength')) {
                this.level = parseInt(this.element.getAttribute('data-strength'));
                if (this.level > PasswordField.levels.length)
                    this.level = PasswordField.levels.length - 1;
            } else {
                this.level = 0;
            }
        }

        public getName(): string {
            return this.getElement().name;
        }

        public getType(): string {
            return 'password';
        }

        private static levels = [
            [/^.{3,30}$/],
            [/[!@#$%\^&*()]/, /[a-z]/, /[A-Z]/, /[0-9]/]
        ];

        public getElement(): HTMLInputElement {
            return this.element;
        }

        isValid(): boolean {
            // iterate over all levels
            for (let i = 0; i <= this.level; i++) {
                // iterate over all regex values in that level
                if (PasswordField.levels[i].length > 0)
                    for (let regex of PasswordField.levels[i]) {
                        if (!regex.test(this.getValue()))
                            return false;
                    }
            }
            // all of the required regex tests passed
            return true;
        }

        getValue(): string {
            return this.element.value;
        }

        validate(): void {
            if (this.isValid()) {
                Decorator.withSuccess(this.element, "Hasło odpowiednio skomplikowane");
            } else {
                Decorator.withError(this.element, "Hasło nie jest wystarczająco skomplikowane");
            }
        }
    }
}