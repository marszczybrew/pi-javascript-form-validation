///<reference path="../defs/es6-promise.d.ts"/>
///<reference path="../defs/jquery.d.ts"/>
///<reference path="../decorator/decorator.ts"/>

namespace Validation {
    import Decorator = FormDecorator.Decorator;
    export class ApiField implements ValidatableField {
        private element: HTMLInputElement;
        private source: string;
        private valid: boolean = undefined;
        private timer: any = undefined;

        constructor(element: Node) {
            this.element = <HTMLInputElement> element;
            if (this.element.hasAttribute('data-source')) {
                this.source = this.element.getAttribute('data-source');
            }
        }

        public getName(): string {
            return this.getElement().name;
        }

        public getType(): string {
            return 'api';
        }

        public getElement(): HTMLInputElement {
            return this.element;
        }

        public isValid(): boolean {
            return this.valid !== undefined && this.valid;
        }

        public getValue(): string {
            return this.element.value;
        }

        public validate(): void {
            if (this.timer === undefined)
                this.timer = window.setTimeout(() => {
                    this.testApi().then(
                        (response) => {
                            this.timer = undefined;
                            if (response) {
                                Decorator.withSuccess(this.element, 'Nazwa poprawna i wolna');
                            } else {
                                Decorator.withError(this.element, 'Nazwa jest zajęta');
                            }
                        },
                        (error) => {
                            this.timer = undefined;
                            console.log(error);
                            Decorator.withError(this.element, 'Błąd podczas komunikacji z api');
                        });
                }, 300);
        }

        private getUrl(): string {
            return this.source + this.getValue();
        }

        private testApi(): Promise<boolean> {
            this.valid = undefined;
            return new Promise((resolve, reject) => {
                $.get(this.getUrl(), {}, function (data) {
                    for (let i in data) {
                        resolve(data[i]);
                    }
                });
            }).then(() => this.valid = true, () => this.valid = false);
        }
    }
}