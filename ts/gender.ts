namespace Validation {
    export enum Gender {
        Man, Woman, Unspecified
    }
}