namespace FormDecorator {
    export class Decorator {
        public static withError(field: HTMLElement, msg: string): void {
            Decorator.removeMessages(field);

            let n = document.createElement("label");
            field.classList.remove('with-success');
            field.classList.add('with-error');
            field.classList.add('animated rubberBand');
            window.setTimeout(() => {
                field.classList.remove('animated rubberBand');
            }, 200);
            n.classList.add('error');
            n.setAttribute('for', '#' + field.id);
            n.innerHTML = msg;
            n.style.color = 'red';
            field.parentElement.appendChild(n);
        }

        public static withSuccess(field: HTMLElement, msg: string): void {
            Decorator.removeMessages(field);

            let n = document.createElement("label");
            field.classList.remove('with-error');
            field.classList.add('with-success');
            n.classList.add('success');
            n.setAttribute('for', '#' + field.id);
            n.innerHTML = msg;
            n.style.color = 'green';
            // field.parentElement.appendChild(n);
        }

        private static removeMessages(elem: HTMLElement) {
            let label = elem.parentElement.querySelector('[for="#' + elem.id + '"]');
            if (label !== null)
                label.remove();
        }
    }
}